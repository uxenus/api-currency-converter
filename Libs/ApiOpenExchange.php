<?php

namespace Libs;

use GuzzleHttp\Exception\GuzzleException;

class ApiOpenExchange extends ApiIntegration
{

    private static $params = [
        '_apiUrl' => 'https://openexchangerates.org/api/latest.json?app_id=',
        '_apiKey' => 'ff59497c49e7430fbda0e0209a663507',
        '_sendMethod' => 'GET'
    ];


    /**
     * ApiOpenExchange constructor.
     */
    public function __construct()
    {
        parent::__construct(self::$params);

    }

    /**
     * @param string $currency1
     * @param string $currency2
     * @return array|bool
     */
    public function getCurrencyRates(string $currency1, string $currency2)
    {

        $currency1 = mb_strtoupper($currency1);
        $currency2 = mb_strtoupper($currency2);

        if ($currency1 === $currency2) {
            return 1; // =)
        }


        $param['sendURL'] = self::$params['_apiUrl'] . self::$params['_apiKey']; // not a free

//        $param['sendURL'] = self::$params['_apiUrl'] . self::$params['_apiKey'] . "&base=" . $currency1; // not a free - so only USD base currency

        try {
            $response = json_decode($this->sendRequest($param), true);
        } catch (GuzzleException $e) {
            return false;
        }

        if (isset($response['rates'])) {
            $rateIs = $response['rates'][$currency2];
        }
        else {
            $rateIs = false;
        }

        return $rateIs;

    }

    /**
     * @return array|bool
     */
    public function getCurrencyRatesAvailable()
    {

        $param['sendURL'] = self::$params['_apiUrl'] . self::$params['_apiKey'];

        try {
            $response = json_decode($this->sendRequest($param), true);
        } catch (GuzzleException $e) {
            return false;
        }

        if (isset($response['rates'])) {
            $rateIs = array_keys($response['rates']);
        } else {
            $rateIs = false;
        }

        return $rateIs;

    }

    /**
     * @return bool|mixed
     */
    public function getCurrenciesSignsAndNames()
    {
        $param['sendURL'] = 'https://openexchangerates.org/api/currencies.json';

        try {
            $response = json_decode($this->sendRequest($param), true);
        } catch (GuzzleException $e) {
            return false;
        }

        return $response;
    }

}