<?php

namespace Libs;

use GuzzleHttp\Exception\GuzzleException;

class ApiCurrencyConverter extends ApiIntegration
{

    private static $params = [
        '_apiUrl' => 'http://free.currencyconverterapi.com/api/v5/convert?q=',
        '_sendMethod' => 'GET'
    ];


    /**
     * ApiCurrencyConverter constructor.
     */
    public function __construct()
    {
        parent::__construct(self::$params);

    }

    /**
     * @param string $currency1
     * @param string $currency2
     * @return array|bool
     */
    public function getCurrencyRates(string $currency1, string $currency2)
    {

        $currency1 = mb_strtoupper($currency1);
        $currency2 = mb_strtoupper($currency2);

        if ($currency1 === $currency2) {
            return 1; // =)
        }

        $parameter = $currency1 . '_' . $currency2;

        $param['sendURL'] = self::$params['_apiUrl'] . $parameter . '&compact=y';

        try {
            $response = json_decode($this->sendRequest($param), true);
        } catch (GuzzleException $e) {
            return false;
        }

        return $response[$parameter]['val'] ?? false;

    }


    /**
     * @return bool|mixed
     */
    public function getCurrencyRatesAvailable()
    {
        $param['sendURL'] = 'https://free.currencyconverterapi.com/api/v6/currencies';

        try {
            $response = json_decode($this->sendRequest($param), true);
        } catch (GuzzleException $e) {
            return false;
        }

        return array_keys($response['results']);
    }
}