<?php

namespace Libs;

use GuzzleHttp\Exception\GuzzleException;

class ApiPrivatBank extends ApiIntegration
{

    private static $params = [
        '_apiUrl' => 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5',
        '_sendMethod' => 'GET'
    ];

    /**
     * ApiPrivatBank constructor.
     *
     */
    public function __construct()
    {
        parent::__construct(self::$params);
    }

    /**
     * @param string $currency1
     * @param string $currency2
     * @return array|bool|mixed
     */
    public function getCurrencyRates(string $currency1, string $currency2)
    {

        try {
            $response = json_decode($this->sendRequest(), true);
        } catch (GuzzleException $e) {
            return false;
        }

        $outData = array_values(array_filter($response, function ($item) use ($currency1, $currency2) {
            if (
                ($item['ccy'] === $currency1 && $item['base_ccy'] === $currency2)
                ||
                ($item['ccy'] === $currency2 && $item['base_ccy'] === $currency1)
            ) {
                return true;
            }
        }));

        return $outData[0]['sale'] ?? false;
    }

    /**
     * @return mixed
     */
    public function getCurrencyRatesAvailable()
    {
        try {
            $response = json_decode($this->sendRequest(), true);
        } catch (GuzzleException $e) {
            return false;
        }

        $response = array_filter($response, function ($itm) {
            if (\in_array('USD', $itm, true)) {
                return true;
            }
        });


        $response = array_map(function ($item) {
            return $item['ccy'] === 'USD'
                ? $item['base_ccy']
                : $item['ccy'];
        }, $response);

        return array_values($response);
    }
}