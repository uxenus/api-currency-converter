<?php

namespace Libs;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;


abstract class ApiIntegration
{

    private static $params;

    /**
     * Integration constructor.
     * @param $params
     */
    public function __construct($params)
    {
        if ($params === null) {
            $params = [
                '_apiUrl' => '',
                '_sendMethod' => 'GET'
            ];
        }

        self::setParams($params);
    }

    /**
     * @param $integrationName
     * @return mixed
     */
    public static function init($integrationName)
    {
        $integrationName = __NAMESPACE__ . "\\" . $integrationName;

        return new $integrationName(self::getParams());
    }


    /**
     * @param string $currency1
     * @param string $currency2
     * @return mixed
     */
    abstract public function getCurrencyRates(string $currency1, string $currency2);

    /**
     * @return mixed
     */
    abstract public function getCurrencyRatesAvailable();

    /**
     * @param $params
     * @return bool|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendRequest(array $params = [])
    {
        $defParams = array(
            'sendURL' => self::$params['_apiUrl'],
            'sendMethod' => self::$params['_sendMethod']
        );

        $params = array_merge($defParams, $params);

        $response = null;
        $client = new Client();

        try {
            
            $request = new Request($params['sendMethod'], $params['sendURL']);
            $response = $client->send($request, ['timeout' => 2]);

        } catch (RequestException $e) {

            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }

            return false;
        }

        if ($response->getStatusCode() === 200) {
            return $response->getBody();
        }

        return false;
    }

    /**
     * @param mixed $params
     */
    public static function setParams($params)
    {
        self::$params = $params;
    }

    /**
     * @return mixed
     */
    public static function getParams()
    {
        return self::$params;
    }


}