<?php

require 'vendor/autoload.php';


$headers = [
    'Access-Control-Allow-Origin' => '*',
    'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
    'Access-Control-Allow-Credentials' => 'true',
    'Access-Control-Max-Age' => '86400',
    'Access-Control-Allow-Headers' => 'Content-Type, Authorization, X-Requested-With'
];

foreach ($headers as $headerName => $headerValue) {
    header($headerName . ': ' . $headerValue);
}

$headers = apache_request_headers();
$method = $_SERVER['REQUEST_METHOD'];

if ($method !== 'OPTIONS') {
    if ($headers['userName'] !== 'api-test-user' || $headers['userPassword'] !== 'api-test-password') {
        exit();
    }
}

use Libs\ApiIntegration;

/* ====================== */

if ($method === 'OPTIONS') {
    $responseBody = json_encode(['method' => 'OPTIONS']);
} else {

    $name = addslashes(strip_tags(htmlentities($_GET['service']))); // =)
    $CCY = addslashes(strip_tags(htmlentities($_GET['ccy']))); // =)
    $mode = addslashes(strip_tags(htmlentities($_GET['mode']))); // =)

    if (isset($_GET['bcy'])) {
        $BASEcy = addslashes(strip_tags(htmlentities($_GET['bcy']))); // =)
    } else {
        $BASEcy = 'USD';
    }

    /* --------------------------------------------- */
    switch ($name) {
        case 'pr':
            $seviceApiName = 'ApiPrivatBank';
            break;
        case 'cc':
            $seviceApiName = 'ApiCurrencyConverter';
            break;
        case 'oe':
            $seviceApiName = 'ApiOpenExchange';
            break;
        default;
            exit(1);
    }

    $apiIntegration = ApiIntegration::init($seviceApiName);
    /* --------------------------------------------- */

    if ($mode === 'ccy-available') {
        $data = $apiIntegration->getCurrencyRatesAvailable();
    } else {
        $data = $apiIntegration->getCurrencyRates($BASEcy, $CCY);
    }


    $responseBody = json_encode(['result' => $data]);
}

header('HTTP/1.1 200 OK');
header('Content-Type:text/html; charset=UTF-8');

echo $responseBody;

